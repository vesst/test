<!DOCTYPE HTML>
<html>
	<head>
		<title>Пример</title>
	</head>
	<body>
		<form action="index.php" method="POST">
			<input type="text" name="url" placeholder="Введите URL">
			<input type="text" name="custom" placeholder="Поле ввода кастомной ссылки">
			<input type="submit" value="Сгенерировать">
		</form>
		<?php
			$_res = htmlspecialchars($_POST['url']);
			$_custom = htmlspecialchars($_POST['custom']);
			$_arr = explode(".", $_res, 3);
			if ($_custom) {
				echo 'Ваша ссылка: <a href=', $_res, '> ', $_custom, '</a>';
			} else {
				echo 'Ваша ссылка: <a href=', $_res, '> ', $_arr[1], '</a>';
			}
		?>
	</body>
</html>